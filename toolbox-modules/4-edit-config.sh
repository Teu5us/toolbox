action="Edit config"
choices+="$action\n"
actions["$action"]=run_editor

configlist="$module_dir/resources/configlist"

list_configs() {
	cat "$configlist" | dmenul -p "Choose config:" | filter_tail
}

list_editors() {
	echo -e "Vim\nEmacs" | dmenul -p "Choose editor:"
}

run_editor() {
	config=$(list_configs)
	[ -z "$config" ] && exit
	case $(list_editors) in
		Vim) $TERMINAL -e sh -c "nvim ${config} && rm .nvimlog" ;;
		Emacs) ecl "$config" ;;
	esac
}
