action="Web search"
choices+="$action\n"
actions["$action"]=run_browser

searchenginelist="$module_dir/resources/searchenginelist"

declare -A engines=()

makeengines() {
	IFS=$'\n'
	for line in $(cat "$searchenginelist")
	do
		engines["$(echo $line | filter_head)"]=$(echo $line | filter_tail)
	done
}

list_search_engines() {
	cat "$searchenginelist" | filter_head | dmenul -p "Choose search engine:"
}

uri(){
	echo "$(dmenu -p 'Enter your search:')" \
	| curl -Gso /dev/null -w %{url_effective} \
	--data-urlencode @- "" | sed -E 's/..(.*).../\1/'
}

run_browser() {
	makeengines
	engine=${engines["$(list_search_engines)"]}
	[ -z "$engine" ] && exit
	query=$(uri)
	[ -z "$query" ] && exit
	$BROWSER "$engine$query"
}
