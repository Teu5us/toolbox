action="Open files"
choices+="$action\n"
actions["$action"]=open_files

file_assoc="$module_dir/resources/file_assoc"

file_browser="thunar"

exts=( $(cat $file_assoc | filter_head) )
declare -A fileassoc=()

makeassoc() {
	IFS=$'\n'
	for line in $(cat "$file_assoc")
	do
		fileassoc["$(echo $line | filter_head)"]="xargs $(echo $line | filter_tail)"
	done
}

list_files() {
	fd_command f --type d | dmenul -p "Choose files:"
}

filter_files_by_ext() {
	IFS=$'\n'
	fileext() {
		echo ${files[$i]} | sed 's#.*\.\(.*\)#\1#'
	}
	for i in ${!files[@]}; do
		if [ "$(fileext)" = "$ext" ]; then
			files_by_ext[$ext]+="\"${files[$i]}\"\n"
			unset 'files[i]'
		fi
	done
}

filter_dirs() {
	IFS=$'\n'
	for i in ${!all_files[@]}; do
		if [ -d "${all_files[$i]}" ]; then
			directories+=(${all_files[$i]})
			unset 'all_files[i]'
		fi
		if [ -f "${all_files[$i]}" ]; then
			files+=(${all_files[$i]})
			unset 'all_files[i]'
		fi
	done
}

make_no_ext() {
	IFS=$'\n'
	for i in ${!files[@]}; do
		no_ext+="\"${files[$i]}\"\n"
		unset 'files[i]'
	done
}

open_files() {
	makeassoc
	all_files=( $(list_files | sort | uniq) )
	files=()
	directories=()
	no_ext=()
	filter_dirs
	declare -A files_by_ext=()

	for ext in ${exts[@]}; do
		filter_files_by_ext
		files_to_open=${files_by_ext[$ext]}
		[ -n "$files_to_open" ] && \
			echo -ne $files_to_open | eval ${fileassoc["$ext"]} &
	done

	make_no_ext
	[ -n "$no_ext" ] && echo -ne $no_ext | xargs $TERMINAL -e nvim $* && rm .nvimlog &

	for i in ${!directories[@]}; do
		cd ${directories[$i]} && $file_browser &
		unset 'directories[i]'
	done
}
