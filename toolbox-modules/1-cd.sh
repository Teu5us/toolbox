action="Open terminal from directory"
choices+="$action\n"
actions["$action"]=run_term

list_dirs() {
	fd_command d | sort | dmenul -p "Choose directory:"
}

run_term() {
	IFS=$'\n'
	dirs=( $(list_dirs) )
	if [ -n "${dir[@]}" ]; then
		for dir in ${dirs[@]}; do
			cd "$dir" && $TERMINAL &
		done
	fi
}
