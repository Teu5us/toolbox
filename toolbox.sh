#!/usr/bin/env bash

module_dir="$(dirname $(readlink -f "$0"))/toolbox-modules"

declare -A actions=()

choices=()

source "$module_dir/resources/common_funcs"

for module in $(find "$module_dir" -name '*.sh' | sort)
do
	source $module
done

choice_made=$(choice)
[ -n "$choice_made" ] && ${actions["$choice_made"]}
